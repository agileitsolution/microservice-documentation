# Agile Microservice Platform and API documentation

<p><b>Agile Microservice is a development platform to generate, develop and deploy Microservices using Database First Approach and Spring Boot & Apache CXF open source frameworks. Database reverse engineering is done using Liquibase migration tools and persistence of data using spring-data-jpa and hibernate.</b></p>
<p><b>Agile Microservice generates Java code from your database. Agile Microservice builds reach generic APIs to fully acces database data on secure way.</b></p>
<p><b>Agile Microservice modernizes legacy applications by adding API-led connectivity that exposes underlying functionality while hiding the complexity. By adding an abstraction layer, APIs eliminate any tight coupling between applications and systems, facilitating a smoother shift to new and latest technologies.
</b></p>
<p><b>Agile Microservice provides you with Cloud Native applications. They are ‘dynamically managed’, ‘container packaged’, and ‘microservice oriented’. 
<p>These three characteristics allow cloud native applications to be ‘optimized for modern distributed systems environments capable of scaling to tens of thousands of self-healing multi-tenant nodes’.</p>
</b></p>

## Application Life Cycle and Agile Microservice Architecture
![Alt text](Process3.png?raw=true "Agile Microservice Architecture ALC")


[Overall Architecture diagram](docs/Multi-Channel-Data-Integartion-Platform.pdf)

[Business proposal](docs/Agile-IT-Soltions-Platform.pdf)

## Agile Microservice API, Actors and functional layers
![Alt text](APIDiagram.png?raw=true "Description of Actors and Roles in defining API")

### Actors  - Roles
  - Designer designes contract (payload). That is your Service Provider API. It could comply with, for example, some regulatory requirements like PSD2 or GDPR.
  - Mapper transforms Service Provider API to local domain.
All this is done at microservice level. And all implementation is in Abstract Layer. An abstract Layer is built using Template Method Design Pattern.

## Security

### All endpoints are secured by configurable securitiy modules. Security modules support next protocols and Authentication:

 - [ OAuth2 ] (https://en.wikipedia.org/wiki/OAuth#OAuth_2.0) 
 - [ Mutual authentication or two-way authentication] (https://dzone.com/articles/mutual-authentication-two-way-ssl-explained-using) and ( https://en.wikipedia.org/wiki/Mutual_authentication)
 - [ HMAC ] (https://en.wikipedia.org/wiki/HMAC)
 - [ Http Basic Authentication ] (https://en.wikipedia.org/wiki/Basic_access_authentication)

<br/>

### List of Generic RESTFull End point and its API. Generic RestFull end point offers next operations on its Resource

### GET	
        /{appid}/all	
	    /{appid}/count/{columName}/	
	    /{appid}/query/	
	    /{appid}/search/
	    /{appid}/search/range/	
	    /{appid}/stringsearch/{type_Equals_or_Like}/
	    /{appid}/{id}/	
### POST
    	/{appid}	
### PUT
    	/{appid}/{id}	
### PATCH
    	/{appid}/update/{id}/	
### DELETE
    	/{appid}/{id}	

**)  Note "/query/" and "/search/" API have the same functionality. They return Collection for input arguments provided. Their difference is in JPA implementation. One is using Hibernate Find By Example (https://dzone.com/articles/hibernate-query-example-qbe) and second one is using JPQL Query ( https://thoughts-on-java.org/jpql/ ). Latter one is much faster.

**) Note Query "/stringsearch/{type_Equals_or_Like}/"" is providing similar Collection.  

**) An {appid}  represents multitenant identifier.



```
/stringsearch/0/ provides the same result for the same parameters while
/stringsearch/1/ is using SQL 'like' criteria to bring Collection of results

```
<br/>

### Resource is an Entity that has field with the name "id" defined. "id" field represents column with Primary Key. 

##  DDL and table structure

### Description of Sample table

![Alt text](DDL_audit_detail.png?raw=true "DDL - Data Model")


#### ERD of Sample Table 

![Alt text](ERD_audit_detail.png?raw=true "ERD")


### Table Sample data
![Alt text](Table_audit_detail.png?raw=true "Sample Table Data")

###  API 


#### API Map
![Alt text](API-map.png?raw=true "RestFul API Map")


#### Some sample URLs

![Alt text](API-URL-samples.png?raw=true "API Map With Sample")


### Generic Resource and Generic API diagram
#### It depicts generic Resource ( Entity with id as a primary key ) and its API ( queries, insert,update, delete)

![Alt text](Resource&API2.png?raw=true "API & Resource")

### Sample API for audit_detail Resource and APP001 Multitenant id

```

https://localhost.ie:8443/api/audit_detail/APP001/all
https://localhost.ie:8443/api/audit_detail/APP001/count/action
https://localhost.ie:8443/api/audit_detail/APP001/query?action=action&field1=1
https://localhost.ie:8443/api/audit_detail/APP001/search?action=action&field1=1
https://localhost.ie:8443/api/audit_detail/APP001/search/range?action.gt=action&action.lt=stop&field2.lt=10&field2.eq=1
https://localhost.ie:8443/api/audit_detail/APP001/stringsearch/1?action=act&field1=1
https://localhost.ie:8443/api/audit_detail/APP001/1
https://localhost.ie:8443/api/audit_detail/APP001/
https://localhost.ie:8443/api/audit_detail/APP001/1
https://localhost.ie:8443/api/audit_detail/APP001/update/2?action=akcija&action_by=dasa&attribute1=peka
https://localhost.ie:8443/api/audit_detail/APP001/1

```

### Detail explanation of <b>/search/range</b>  API

It could have 5 possible values after <b>$COLUMN_NAME.</b>

- gt   =  greater than</p>
- gte  =  greater than or equals</p>
- lt   =  less than</p>
- lte  =  less than or equals</p>
- eq   =  equals</p>

 
For URL below
https://localhost.ie:8443/api/audit_detail/APP001/search/range?action.gt=ACTION_START&action.lt=ACTION_STOP&field2.lt=10&field2.eq=1

```
action.gt=action  => action > "ACTION_START"
action.lt=stop    => action < "ACTION_STOP" 
field2.lt=10      => field2 < 10 
field2.eq=1       => field2 = 1 
```
But also combination could be 

```
URL Reference		    SQL representations
-------------------------------------------------- 
$COLUMN_NAME.GTE=$VALUE  => $COLUMN_NAME>= $VALUE
$COLUMN_NAME.LTE=$VALUE  => $COLUMN_NAME<=$VALUE
$COLUMN_NAME.EQ =$VALUE  => $COLUMN_NAME=$VALUE

```

### Scaling (YouTube Presentation)
[![Scaling Microservices](https://img.youtube.com/vi/8TgaMc4LOk4/2.jpg)](https://www.youtube.com/watch?v=8TgaMc4LOk4 "Scaling Microservices")

### IoT MIcroservices Sample (YouTube Presentation)
[![IoT Microservices](https://img.youtube.com/vi/zSgB9jbGBao/3.jpg)](https://www.youtube.com/watch?v=zSgB9jbGBao "IoT Microservices")



## Old Architectural Diagrams 

![Alt text](Enterprise-Integration.jpg?raw=true "Enterprise Data Integration")

![Alt text](EIS-Generation.png?raw=true "Generation and Deploying")
