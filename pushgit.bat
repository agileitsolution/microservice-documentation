@rem Copyright (c) 2018 Agile IT Solutions. All rights reserved.
@rem git add, commitand push batch file
@echo off

IF NOT "%~1"=="" GOTO START
ECHO This script requires the next parameters:
ECHO - message to commit with
ECHO Examples:
ECHO pushgit.bat "removing obsolete code"
GOTO :EOF

:START
echo %~1
git status
git add .
git commit -m "%~1"  
git push origin
:END
